import sys
import os
import platform
import json
import requests


def main(argv):
    if len(argv) < 3 or len(argv) > 5:
        sys.stderr.write("Usage: {} <uuid> <skinfile> [steve|normal|slim|alex] [minecraftdir]".format(argv[0]))
        return 1

    playeruuid = argv[1].replace("-", "").lower()
    skinfile = argv[2]
    slimarms = len(argv) >= 4 and (argv[4] == 'slim' or argv[4] == 'alex')
    mcdir = getmcdir() if len(argv) < 5 else argv[5]

    if len(argv) == 3:
        print("Minecraft game directory detected as {}".format(mcdir))
        print("If this is incorrect, you may specify the Minecraft directory manually.")
    else:
        print("Minecraft game directory manually set as {}".format(mcdir))
    print("UUID: {}, Arm type: {}, Skin file: {}".format(playeruuid, "Slim/Alex" if slimarms else "Normal/Steve", skinfile))
    print("")

    print("MCSkinUploader by figboot (bigfoot547).")
    print("Licensed under the BSD 3-Clause. See LICENSE.txt for more details.")
    print("Copyright (c) 2020 bigfoot547, figboot. All rights reserved.")
    print("")

    mojtoken = None
    try:
        with open(mcdir + 'launcher_profiles.json', 'r') as profiles:
            print("Loading launcher profiles...")
            data = json.load(profiles)
            print("Grabbing Mojang access token...")

            for authProfile in data['authenticationDatabase']:
                profiledata = data['authenticationDatabase'][authProfile]
                if 'profiles' in profiledata:
                    for mcProfile in profiledata['profiles']:
                        if mcProfile.lower() == playeruuid:
                            print("Correct Minecraft account found among launcher profiles!")
                            mojtoken = profiledata['accessToken']
                        if mojtoken is not None:
                            break
                else:
                    continue
                if mojtoken is not None:
                    break
    except FileNotFoundError:
        sys.stderr.write("Launcher profiles file not found! Make sure your Minecraft directory was correctly detected,"
                         " otherwise set it manually via the command line.")
        return 1
    except IOError:
        sys.stderr.write("An error occurred while loading launcher profiles.")
        return 1
    except KeyError:
        sys.stderr.write("Error traversing launcher profiles. You have a weirdly formatted launcher profiles file.")
        return 1

    if mojtoken is None:
        sys.stderr.write("A valid Mojang access token could not be found. Have you logged into this account"
                         " via the launcher?")
        return 1

    print("Validating access token...")
    if not api_validatetoken(mojtoken):
        sys.stderr.write("Access token was found to be invalid. Try logging into this account via the launcher again.")
        return 1

    print("Token is valid, uploading skin.")
    try:
        with open(skinfile, 'rb') as skin:
            if not api_uploadskin(mojtoken, playeruuid, skin.read(), slimarms):
                sys.stderr.write("An error occurred while uploading the skin.")
                return 1
    except IOError as ex:
        sys.stderr.write("An IO error occurred while loading the skin file. {}".format(ex))
        return 1

    profile = api_getprofile(playeruuid)

    if profile is None:
        sys.stderr.write("An error occurred while querying the player's profile. This could be due to rate-limiting.")
        exit(1)
    else:
        print("Skin data acquired! Removing skin...")

    if api_deleteskin(mojtoken, playeruuid):
        print("Skin deleted, I think we're done here.")
    else:
        sys.stderr.write("An error occurred while removing the skin from the profile.")
        # We got the skin data, we just couldn't delete the skin. Let's not exit.

    print("")
    print("Texture data: ")
    print(profile[0])
    print("")
    print("Signature: ")
    print(profile[1])

    return 0


def getmcdir():
    plat = platform.system().lower()

    if plat == "windows":
        return os.getenv("AppData") + "\\.minecraft\\"
    else:
        return os.getenv("home") + "/.minecraft/"


def api_validatetoken(token: str):
    res = requests.post("https://authserver.mojang.com/validate", json={'accessToken': token})
    res.close()
    return res.status_code == 204  # Will return 204 No Content if it is valid, 403 Not Authorized (or other) if invalid.


def api_uploadskin(token: str, uuid: str, skindata: bytes, slim: bool):
    headers = {
        'Authorization': "Bearer " + token
    }

    files = {
        'model': 'slim' if slim else '',
        'file': skindata
    }
    res = requests.put("https://api.mojang.com/user/profile/{uuid}/skin".format(uuid=uuid), headers=headers, files=files)
    res.close()
    if res.status_code != 200:
        print(res.status_code, res.text)
    return res.status_code == 200


def api_deleteskin(token: str, uuid: str):
    headers = {
        'Authorization': "Bearer " + token
    }

    res = requests.delete("https://api.mojang.com/user/profile/{uuid}/skin".format(uuid=uuid), headers=headers)
    res.close()
    return res.status_code == 200 or res.status_code == 204


def api_getprofile(uuid: str):
    res = requests.get("https://sessionserver.mojang.com/session/minecraft/profile/{uuid}?unsigned=false".format(uuid=uuid))
    res.close()
    if res.status_code == 200:
        data = res.json()
        if 'properties' in data:
            properties = data['properties']
            for profileprop in properties:
                if profileprop['name'] == "textures":
                    return profileprop['value'], profileprop['signature']

    return None


if __name__ == "__main__":
    exit(main(sys.argv))
